#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-

from __future__ import print_function

import pika
import json
import sys

from lc_ldap.shortcuts import lc_ldap_admin
from affich_tools import prompt
import lc_ldap.filter2 as filter

from client import Ticket

ldap = lc_ldap_admin()

conf_wifi_only = True
conf_reset_password = False
conf_filter = None

for arg in sys.argv[1:]:
    if arg == '--all':
        conf_wifi_only = False
    elif arg == '--pass':
        conf_reset_password = True
    elif arg == '--debug':
        pass
    elif arg.startswith('--'):
        print("Unknown arg")
        exit(12)
    else:
        conf_filter = arg

f = filter.human_to_ldap(conf_filter.decode('utf-8'))
res = ldap.search(f, mode='rw')
if not conf_filter:
    print("Give a filter !")
    exit(3)
elif len(res) > 1:
    print("More than one result")
    exit(1)
elif not res:
    print("Nobody found")
    exit(2)
else:
    item = res[0]
    if 'uid' not in item:
        conf_reset_password = False
        
    item.display()
    if conf_reset_password:
        print("Le mot de passe (compte Crans) sera également réinitialisé")

    while True:
        c = prompt("[O/N]").lower()
        if c == 'n':
            exit()
        elif c == 'o':
            break

ticket = Ticket()
if 'uid' in item and conf_reset_password:
    ticket.reset_password(item)

if item.ldap_name is 'facture':
    ticket.add_facture(item)
elif hasattr(item, 'machines'):
    for m in item.machines():
        if not conf_wifi_only or 'machineWifi' in m['objectClass']:
            ticket.add_machine(m)
else:
    ticket.add_machine(item)

ticket.print()

