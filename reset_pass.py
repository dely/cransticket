#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-

from __future__ import print_function
import sys
from lc_ldap import crans_utils
from lc_ldap.shortcuts import lc_ldap_admin
from client import Ticket

conn = lc_ldap_admin()

if len(sys.argv) != 2:
    print("Veuillez taper UN login")
    exit(1)
login = sys.argv[1]
adh = conn.search(u'uid=%s' % crans_utils.escape(login), mode='rw')
if not adh:
    print("Adhérent introuvable")
    exit(1)
adh = adh[0]

ticket = Ticket()
ticket.reset_password(adh)
ticket.print()
