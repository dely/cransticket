#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import sys
# Bad namming: change namming
from AdafruitThermal import Adafruit_Thermal as AdafruitThermal
import subprocess, time, Image, socket
import os
import json
from config import DEVICE

#
CODING='cp437'

SAMPLE_MACHINE = {
  'host': 'nouille',
  'macAddress': '<automatique>',
  'secret': '**********',
}

SAMPLE_ACCOUNT = {
  'login': 'passoir',
  'pass': 'omgverysecure',
}

# Load data
crans_logo = Image.open(os.path.join(os.path.dirname(__file__), 'logo_crans.png'))


printer = AdafruitThermal(DEVICE, 19200, timeout=5)

def print_carac(text, value):
    printer.justify('L')
    pad = 384/12 - len(text) - len(value)
    printer.println(text + ('.'*pad) + value )


def show_entry(entry):
    # Si le champ fid est présent, c'est une facture, on utilise une fonction
    # annexe
    if 'fid' in entry.keys():
        show_facture(entry)
        return
    printer.justify('C')
    printer.underlineOn()
    if 'host' in entry:
        title = u'Détails machine'
        if 'type' in entry:
            title += u' ' + entry['type']
        printer.println(title.encode(CODING))
    else:
        printer.println(u'Compte Crans'.encode(CODING))
    printer.underlineOff()
    printer.justify('L')

    login = entry.get('login', None) or entry['host']
    print_carac('Login', login)
    if 'macAddress' in entry:
        print_carac('Mac', entry['macAddress'])
    if 'secret' in entry:
        print_carac('Mot de passe', entry['secret'])
    if 'pass' in entry:
        print_carac('Mot de passe', entry['pass'])
    
    printer.feed(1)


def make_fact_elem(element,nb_car,is_last):

    ligne = u''

    u_elem = unicode(element)
    if len(u_elem) >= nb_car:
        ligne += u_elem[0:nb_car]
    else:
        if is_last:
            for i in range(nb_car-len(u_elem)):
                ligne += u' '
            ligne += u_elem
        else:
            ligne += u_elem
            for i in range(nb_car-len(u_elem)):
                ligne += u' '

    if is_last:
        ligne += u''
    else:
        ligne += u'|'

    return ligne

# Fonction spéciale pour l'impression des factures
# ! facture est un dictionnaire, pas un objet LDAP !
def show_facture(facture):

    #Impression de l'en-tête
    printer.justify('L')
    printer.println(facture['recuPaiement'].encode(CODING))
    printer.justify('C')
    printer.println(u'--------------------------------'.encode(CODING))
    printer.boldOn()
    fid = u'Facture n°%d' % facture['fid']
    printer.println(fid.encode(CODING))
    printer.boldOff()
    printer.println(u'--------------------------------'.encode(CODING))

    #Impression de l'en-tête de la facture
    nom_complet = facture['nom'] + u' ' + facture['prenom']
    printer.justify('R')
    printer.println((u'Adhérent : ' + nom_complet).encode(CODING))
    if facture['chbre'] is u'EXT':
        printer.println(u'Externe'.encode(CODING))
    else:
        printer.println((u'Chambre : ' + facture['chbre']).encode(CODING))

    printer.println((u'Début adhésion : ' + facture['debutAdhesion']).encode(CODING))
    printer.println((u'Fin adhésion : ' + facture['finAdhesion']).encode(CODING))
    printer.println((u'Fin connexion : ' + facture['finConnexion']).encode(CODING))

    printer.justify('L')
    printer.println(u'--------------------------------'.encode(CODING))
    printer.println(u' Code |   Désig.  |Qté|   P.U   '.encode(CODING)) #Code(6c. max.),Désignation(11c. max.),Qté(3c. max),PU(8[+1]c.max)
    printer.println(u'------|-----------|---|---------'.encode(CODING))

    #Impression du corps de la facture + calcul du prix total
    total = 0.0
    for art in facture['article']:
        row = u''
        row += make_fact_elem(art['code'],6,False)
        row += make_fact_elem(art['designation'],11,False)
        row += make_fact_elem(art['nombre'],3,False)
        row += make_fact_elem(art['pu']+u'e',9,True)
        total += float(art['pu'])

        printer.println(row.encode(CODING))

    printer.println(u'--------------------------------'.encode(CODING))

    #On affiche le total
    printer.justify('R')
    printer.println((u'---------').encode(CODING))
    printer.println((u'Total à régler  '+ '|' + make_fact_elem(total,8,True) + u'e').encode(CODING))
    printer.println((u'---------').encode(CODING))
    printer.justify('L')
    printer.println((u'Payé par ' + facture['modePaiement']).encode(CODING))

    #On met une super phrase sur la TVA
    printer.feed(1)
    printer.println(u'TVA non applicable(art. 293B du CGI)'.encode(CODING))


    #Fin de la facture
    printer.feed(1)


# Do print
def print_liste(liste):
    printer.setDefault() # Restore printer to defaults
    printer.printImage(crans_logo, True)
    
    for entry in liste:
        show_entry(entry)
    
    if any('secret' in entry or 'pass' in entry for entry in liste):
        printer.println(u'Veuillez conserver ces'.encode(CODING))
        printer.println(u'informations en lieu sûr.'.encode(CODING))
        printer.println(u' '.encode(CODING))
        printer.println(u' '.encode(CODING))
    
    printer.feed(2)

if __name__ == '__main__':
    if not sys.argv[1:]:
        liste = [SAMPLE_ACCOUNT, SAMPLE_MACHINE]
    else:
        with open(sys.argv[1], 'r') as f:
            liste = json.load(f)
    print_liste(liste)

