# Crans Ticket

## Matériel
 * Une raspberry pi modèle B
 * Une carte SD
 * Alim 5V (beaucoup d'A ?)
 * Prise femelle (pour cette alim)
 * Câble micro-usb
 * [Batterie de secours](http://www.priceminister.com/offer/buy/364740224/urbanrevolt-19857.html) (24€)
 * [Imprimante thermique](http://www.adafruit.com/products/597) (~50€)
 * Boîte de ferrero

## Installation du dépôt sur l'imprimante

Paquets nécessaires à cransticket :

    #rajouter wheezy-backports dans sources.list
    apt-get update
    apt-get -t wheezy-backports python-pika
    apt-get install python-imaging

Pour le dépôt lui-même :

    git clone $le_dépot
    cd $le_dépot
    git submodule init
    git submodule update

On peut tester l'imprimante seule en lançant dump.py.

## Autorisations sur rabbitmq

Ci dessous, le serveur rabbitmq s'appelle "civet" et le client "cransticket"
se nomme "rasputin".

sur civet:
    sudo rabbitmqctl add_user rasputin $mdp
    sudo rabbitmqctl set_permissions rasputin "(amq\.default|CransTicket)" "(amq\.default|CransTicket)" "(amq\.default|CransTicket)"
  
## TODO (à mettre en forme dans la doc)
    sudo rabbitmqctl list_permissionsr
    sudo rabbitmq list_permissions
    sudo rabbitmqctl list_queues
    sudo rabbitmqctl help | less
    sudo rabbitmqctl trace 
    sudo rabbitmqctl trace_on
    sudo less /var/log/rabbitmq/rabbit@civet.log
    sudo rabbitmqctl list_permissions
    sudo rabbitmqctl set_permissions oie ".*" ".*" ".*"
    sudo rabbitmqctl set_permissions oie "CransTicket" "CransTicket" "CransTicket"
    sudo rabbitmqctl set_permissions oie "CransTicket" "CransTicket" "amq\.default"
    sudo rabbitmqctl set_permissions oie "amq\.default" "CransTicket" "amq\.default"
    sudo rabbitmqctl set_permissions oie "amq\.default|CransTicket" "CransTicket" "amq\.default"
    sudo rabbitmqctl set_permissions oie "amq\.default\|CransTicket" "CransTicket" "amq\.default"
    sudo rabbitmqctl set_permissions oie "(amq\.default|CransTicket)" "CransTicket" "amq\.default"
    sudo rabbitmqctl set_permissions oie "\(amq\.default|CransTicket\)" "CransTicket" "amq\.default"
    sudo rabbitmqctl set_permissions oie "(amq\.default|CransTicket)" "CransTicket" "(amq\.default|CransTicket)"
