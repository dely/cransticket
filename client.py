# -*- coding: utf-8 -*-

from __future__ import print_function

from gestion import secrets_new as secrets
import pika
import json
import random
import datetime
import string
from lc_ldap import crans_utils

CREDS = pika.credentials.PlainCredentials('oie', secrets.get('rabbitmq_oie'), True)
import sys
if '--debug' in sys.argv[1:]:
    CREDS = pika.credentials.PlainCredentials('rasputin', secrets.get('rabbitmq_rasputin'), True)

PARAMS = pika.ConnectionParameters(host='rabbitmq.crans.org',
    port=5671, credentials=CREDS, ssl=True)
rabbit_c = pika.BlockingConnection(PARAMS)

ch = rabbit_c.channel()
if '--debug' in sys.argv[1:]:
    QUEUE_NAME = 'CransTicketDebug'
    print("Debug")
else:
    QUEUE_NAME = 'CransTicket'
ch.queue_declare(QUEUE_NAME)

def gen_password():
    """Génère un mot de passe aléatoire"""
    random.seed(datetime.datetime.now().microsecond)
    chars = string.letters + string.digits + '/=+*'
    length = 10
    return ''.join([random.choice(chars) for _ in xrange(length)])

class Ticket(object):
    
    data = []
    
    def add_entry(self, x):
        self.data.append(x)

    def add_account(self, login, mdp):
        self.add_entry({'login': login, 'pass': mdp})

    def add_machine(self, machine):
        todo = {
            'host': machine['host'][0].split('.', 1)[0],
            'macAddress': machine['macAddress'][0].value,
            'type': 'fil',
        }
        if machine.has_key('ipsec'):
            todo['secret'] = machine['ipsec'][0].value
            todo['type'] = 'wifi'
        self.add_entry(todo)

    def add_facture(self, facture):
        proprietaire = facture.proprio()
        chambre = None

        #On vérifie que le propriétaire a une chambre sinon on met EXT
        if 'chbre' in proprietaire.keys():
            chambre = proprietaire['chbre'][0].value
        else:
            chambre = u'EXT'

        deb_adh = crans_utils.datetime_from_generalized_time_format(facture['debutAdhesion'][0].value)
        fin_adh = crans_utils.datetime_from_generalized_time_format(facture['finAdhesion'][0].value)
        fin_co  = crans_utils.datetime_from_generalized_time_format(facture['finConnexion'][0].value)

        todo = {
            'fid' : facture['fid'][0].value,
            'article' : [ art.value for art in facture['article']],
            'modePaiement' : facture['modePaiement'][0].value,
            'recuPaiement' : facture['recuPaiement'][0].value,
            'debutAdhesion' : deb_adh.strftime('%d/%m/%Y'),
            'finAdhesion' : fin_adh.strftime('%d/%m/%Y'),
            'finConnexion' : fin_co.strftime('%d/%m/%Y'),
            'chbre' : chambre,
            'nom' : proprietaire['nom'][0].value,
            'prenom' : proprietaire['prenom'][0].value,
        }
        self.add_entry(todo)

    def reset_password(self, adh):
        login = adh['uid'][0].value
        try:
            value = gen_password()
            with adh:
                adh['userPassword'] = [crans_utils.hash_password(value).decode('ascii')]
                adh.history_gen()
                adh.save()
        except EnvironmentError:
            print("Impossible de changer le mot de passe de %s" % login)
            exit(2)
        self.add_account(login, value)

    def print(self):
        if not self.data:
            print("Nothing to print !")
            return
        ch.basic_publish(exchange='', routing_key=QUEUE_NAME,
            body=json.dumps(self.data))
        print("Un nouveau ticket est en cours d'impression ...")
    
